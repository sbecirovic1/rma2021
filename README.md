##### Elektrotehnički fakultet u Sarajevu
##### Računarstvo i informatika/Stručni studij Razvoj softvera
#### Razvoj mobilnih aplikacija 

 ```
 Na ovom repozitoriju će se nalaziti rješenja zadataka sa vježbi na predmetu Razvoj mobilnih aplikacija
 ```
 
[Vježba 2](https://bitbucket.org/sbecirovic1/rma2021/src/Vje%C5%BEba2/)

[Vježba 3](https://bitbucket.org/sbecirovic1/rma2021/src/Vje%C5%BEba3/)

[Vježba 4](https://bitbucket.org/sbecirovic1/rma2021/src/Vjezba4/)

[Vježba 5](https://bitbucket.org/sbecirovic1/rma2021/src/Vjezba5/)

[Vježba 6](https://bitbucket.org/sbecirovic1/rma2021/src/Vjezba6/)

[Vježba 7](https://bitbucket.org/sbecirovic1/rma2021/src/Vjezba7/)

[Vježba 8](https://bitbucket.org/sbecirovic1/rma2021/src/Vjezba8/)

[Vježba 9](https://bitbucket.org/sbecirovic1/rma2021/src/Vjezba9/)

[Vježba 10](https://bitbucket.org/sbecirovic1/rma2021/src/master/)

Ključ se nalazi u gradle.properties.Da biste mogli pristupati ključu mora se izvršiti build aplikacije.
